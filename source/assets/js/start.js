(function( $ ) {
/**
 * START - ONLOAD - JS
 * Updated by mrok93
 * Date: 11-11-2016
 * 
 * 1. Slider                : slider()
 * 2. Sort projects         : sortProj()
 * 3. Testimonial Slider    : testiSlider()
 * 4. fixMenu()             : Fix menu when user scrolls down
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
// 1. Slider: slider()
function slider() {
    if(!$("#main-slider").length) { return; }

    $('#main-slider').flexslider({
        animation: "slide",
        controlNav: false
    });
}

// 2. Sort projects: sortProj()
function sortProj() {
    if(!$(".project-list").length) { return; }

    var $grid = $('.project-list').isotope({
        // options
        itemSelector: '.project-list-itm',
        layoutMode: 'masonry',
        masonry: {
            columnWidth: '.project_sizer'
        }
    });

    $grid.isotope('layout');

    // Filter items on clicking button
    $('.project-btn-group').on( 'click', 'a', function() {
        var filterValue = $(this).attr('data-filter');

        $grid.isotope({ filter: filterValue });
        $('.project-btn-group > li').removeClass("active");
        $(this).parent().addClass("active");
    });
}

// 3. Testimonial Slider: testiSlider()
function testiSlider() {
    // if(!$(".testi-slider").length) { return; }
    $(".testi-slider").flexslider({
        animation: "slide"  
    });
}

// 4. fixMenu(): Fix menu when user scrolls down
function fixMenu() {
    if(!$(".top-header").length) { return; }

    $(window).on("scroll", function(e) {
        if($(window).scrollTop() >= 80) {
            $(".top-header").addClass("fixed");
        } else {
            $(".top-header").removeClass("fixed");
        }
    })
}

/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    slider ();
    
    testiSlider();
    fixMenu();
});
/* OnLoad Window */
var init = function () {
    sortProj();
};
window.onload = init;

})(jQuery);
